s = 'Bonjour, bienvenue sur mon projet.'
print(s)

for c in s:
  print(c)

#s.title()
titre = ("Partie 1 : Les chaines de caractères")
titre2 = ("Partie 2 : Listes")
titre3 = ("Partie 3 : Dictionnaires")
v = titre.title()
v2 = titre2.title()
print(v)

#s.capitalize
string_name = "Marine mEUNIER"
print(string_name.capitalize())

#s.casefold
x = "ß"
y = "ss"
if x.casefold() == y.casefold():
  print(x, "et", y, "c'est la même chose")
else:
  print(x, "et",y, "Ce n'est pas la même chose")

#s.translate
  
print(v2)


## Strings, lists and dictionaries
s, l, d = 'hello', [22, 3.14, 'world'], {'age': 33, 'name': 'Luis'}
l, s, d

## Tuples
t = (22, 3.14, 'hello')
t

# Use triple double quote
haiku1 = """La pluie commence à tomber
c'est le battement
du cœur de la nuit"""

haiku2 = "La pluie commence à tomber\n"
haiku2 += "c'est le battement\n"
haiku2 += "du cœur de la nuit"

haiku3 = ("La pluie commence à tomber\n"
"c'est le battement\n"
"du cœur de la nuit")
j=haiku1 == haiku2 == haiku3

print(j)

#Jeu Pierre Feuille Ciseaux
from random import * 
def choix_joueur1():
    joueur = input("Pierre\nPapier\nCiseaux\n")
    while joueur not in ["pierre","papier","ciseaux"]:
        joueur = input("pierre\npapier\nciseaux\n")
    return(joueur)

def choix_joueur2():
    return choice(["pierre", "papier", "ciseaux"])


print('Entre ton choix en minuscule:') 
a = choix_joueur1()
b = choix_joueur2()
print("Le choix de l'adversaire est :",b)
if a == "pierre" and b == "ciseaux":
    print("Tu as gagné !")
elif a == "pierre" and b == "papier":
    print("Tu as perdu !")
elif a == "pierre" and b == "pierre":
    print("Egalite !")
 
if a == "papier" and b == "pierre":
    print("Tu as gagné !")
elif a == "papier" and b == "ciseaux":
    print("Vous avez perdu !")
elif a == "papier" and b == "papier":
    print("Egalite !")
 
if a == "ciseaux" and b == "papier":
    print("Tu as gagné !")
elif a == "ciseaux" and b == "pierre":
    print("Tu as perdu !")
elif a == "ciseaux" and b == "ciseaux":
    print("Egalite !")


#Faisons des maths

def saisirEntierBorné(bmin, bmax):
    print("Entier dans [",bmin,", ", bmax, "] : ",sep='', end='')
    e = int(input())
    while e<bmin or e>bmax:
        e = int(input("Incorrect ! Nouvel entier : "))
    return e

def afficher(matrice):
    for i in range(len(matrice)):
        print("|", end='')
        for j in range(len(matrice[i])):
            print(matrice[i][j], end='')
        print("|")


print("Nombre de lignes")
n = saisirEntierBorné(1, 5)

print("Nombre de colonnes")
p = saisirEntierBorné(1, 5)


#Création de la matrice

print('Créons une matrice, si vous voulez ne pas perdre trop de temps a tester le code, créez une petite matrice')
mat = [0]*n
for i in range(n):
    mat[i] = [0]*p

print(mat)

#Saisie de la matrice
print("\n--Saisie des éléments--")
for i in range(n):
    for j in range(p):
        print("- Element (", i+1,", ",j+1, sep='')
        mat[i][j] = saisirEntierBorné(-9, 9)

print("\nLa matrice :")
afficher(mat)

#Suite de Fibonacci
nb = int(input("Combien de bombre de la suite de Fibonnacci voulez-vous afficher ? "))
 
t = 0
u = 1
 
print("\n la suite fibonacci est :")
print(t, ",", u, end=", ")
 
for i in range(2, nb):
  suivant = t + u
  print(suivant, end=", ")
 
  t = u
  u = suivant

print('Repassons au cours :')
#dataclass data objects with muttable fields
from dataclasses import dataclass
@dataclass
class Guy:
  name: str
  age: int = 42
g3 = Guy(name='Luis')
print(g3)
# Guy(age=42, name='Luis')
g3.age += 3
g3.age
# 45

#The else clause of for loop
for ik in range(3):
  print(ik)
else:
  print('executed after the end of for iteration')

#Faisons un autre jeu
print('Jouons au pendu')

import random
choix = ["cheval", "poney", "selle", "bride"]
reponse = random.choice(choix)

reponse = "cheval"
nbre = 7
affichage = "" 
trouve = ""

for l in reponse:
  affichage = affichage + "_ "

print(">> Bienvenue dans le pendu <<")

while nbre > 0:
  print("\nMot à deviner : ", affichage)
  proposition = input("proposez une lettre : ")[0:1].lower()

  if proposition in reponse:
      trouve = trouve + proposition
      print("Bonne réponse!")
  else:
    nbre = nbre - 1
    print("Raté\n")
    if nbre==0:
        print(" ==========Y= ")
    if nbre<=1:
        print(" ||/       |  ")
    if nbre<=2:
        print(" ||        0  ")
    if nbre<=3:
        print(" ||       /|\ ")
    if nbre<=4:
        print(" ||       /|  ")
    if nbre<=5:                    
        print("/||           ")
    if nbre<=6:
        print("==============\n")

  affichage = ""
  for x in reponse:
      if x in trouve:
          affichage += x + " "
      else:
          affichage += "_ "

  if "_" not in affichage:
      print("C'est gagné!")
      break
     
print("\nLa partie est terminée")

print('Trouvons le nombre de e dans votre prenom')
msg = "Bonjour " 
prn = input("Quel est ton prenom? ")
print( msg + prn)

compteur = 0
for lettre in prn:
  if x == 'e' or x == 'E':
    compteur = compteur + 1
print("Nombre de e :",compteur)

liste = ""
for y in prn:
  if y not in liste:
    liste = liste + y
print("Lettres utilisées :",liste + " ")

## string API
string_api = """
s.capitalize
s.casefold
s.center
s.count
s.encode
s.endswith
s.expandtabs
s.find
s.format
"""
   
## list API
list_api = """
l.append  l.count   l.insert  l.reverse
l.clear   l.extend  l.pop     l.sort


l.copy    l.index
"""
## dict API
dict_api = """
d.clear      d.get
d.copy       d.items
d.fromkeys   d.keys
"""


def plus(x, y=3, nom='luis', age=52, ville=None):
  print(f'Nom: {nom}, Age: {age}, Habite à: {ville}')
  print(f'{x} + {y} = {x+y}')
plus(7, 14, 'a')


def my_plus(x, y=2, *optional_list, **optional_dict):
  "my_plus illustrates a generic python function"

# 'y' has the default value: 2
  print(f'{x} plus {y} is {x+y}')
  # optional_list is ... optional
  if optional_list:
    print(f'list: len: {len(optional_list)} '
          f'items: {optional_list}')
    # optional_dict is ... optional
  if optional_dict:
    print(f'dict:\n'
          f' keys: {list(optional_dict.keys())}\n'
          f' values: {list(optional_dict.values())}')
my_plus(64, 12, 'aa', 'zz', town='Shanghai', note='AAA')


class Pet():
  # The constructor is the method named __init__
  def __init__(self, name, food='eggs'):
    # name and food are instance variables
    self.name = name
    self.food = food
  # show_msg is a class variable
  show_msg = "I am {name}, a Pet, I eat {food}"
  # A class method is just a fonction with self as first variable # self refer to the future instance
  def show(self, msg=None):
    ns = dict(name=self.name, food=self.food)
    if not msg:
      print(Pet.show_msg.format(**ns))
    else:
      print(msg.format(**ns))
      
pets = [Pet(name) for name in ['Bob', 'Mick', 'Yan', 'Tony']]
for pet in pets:
  pet.show()
  if len(pet.name) > 3:
    pet.food = 'bacon'
    pet.show(' Hey, now {name} eats {food}')


